#!/bin/bash

### ADD GO user sudoers ###
#mkdir /etc/sudoers.d/admin
#echo "go ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/admin

echo ""
echo "### Vault installation ###"
echo ""
wget https://releases.hashicorp.com/vault/1.13.1/vault_1.13.1_linux_amd64.zip 
unzip vault_1.13.1_linux_amd64.zip -d /usr/local/bin 
rm vault_1.13.1_linux_amd64.zip
vault --version
echo ""
echo "### Vault login ###"
echo ""
vault login s.KeBEluk4he9rQRFwjV39k85U 
mkdir /root/.kube
vault kv get -mount=kv/data -field=kubeconfig kubeconfig > ~/.kube/admin.conf
export KUBECONFIG=~/.kube/admin.conf

echo ""
echo "### Helm installation ###"
echo ""
wget https://get.helm.sh/helm-v3.11.2-linux-amd64.tar.gz 
tar xvf helm-v3.11.2-linux-amd64.tar.gz 
sudo mv linux-amd64/helm /usr/local/bin
rm helm-v3.11.2-linux-amd64.tar.gz
rm -rf linux-amd64
helm version
cat ~/.kube/admin.conf
echo $KUBECONFIG

echo ""
echo "### Openshift installation ###"
echo ""
wget https://github.com/openshift/okd/releases/download/4.5.0-0.okd-2020-07-14-153706-ga/openshift-client-linux-4.5.0-0.okd-2020-07-14-153706-ga.tar.gz 
tar -xvf openshift-client-linux-4.5.0-0.okd-2020-07-14-153706-ga.tar.gz 
sudo mv oc kubectl /usr/local/bin/
oc version

echo ""
echo "### Openshift login ###"
echo ""
oc login -u $OCLOGIN -p $OCPASS
oc status

echo ""
echo "### Helm deploy ###"
echo ""
helm repo add cloudecho https://cloudecho.github.io/charts/
helm repo update
oc create namespace hello
helm install my-hello cloudecho/hello -n hello --set service.type=NodePort
export NODE_PORT=$(kubectl get --namespace hello -o jsonpath="{.spec.ports[0].nodePort}" services my-hello) 
export NODE_IP=$(kubectl get nodes --namespace hello -o jsonpath="{.items[0].status.addresses[0].address}") 
echo http://$NODE_IP:$NODE_PORT